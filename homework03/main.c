#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <omp.h>
#include "main.h"

#define MAX_FILENAME 256
#define MAX_NUM_LENGTH 100
#define MAX_ITER 500

#define NUM_TIMERS       6
#define LOAD_TIME        0
#define CONVERT_TIME     1
#define LOCK_INIT_TIME   2
#define SPMV_COO_TIME    3
#define SPMV_CSR_TIME    4
#define STORE_TIME       5


int main(int argc, char** argv)
{
    // program info
    usage(argc, argv);


    // Initialize timess
    double timer[NUM_TIMERS];
    uint64_t t0;
    for(unsigned int i = 0; i < NUM_TIMERS; i++) {
        timer[i] = 0.0;
    }
    InitTSC();


    // Read the sparse matrix file name
    char matrixName[MAX_FILENAME];
    strcpy(matrixName, argv[1]);
    int is_symmetric = 0;
    read_info(matrixName, &is_symmetric);


    // Read the sparse matrix and store it in row_ind, col_ind, and val,
    // also known as co-ordinate format (COO).
    int ret;
    MM_typecode matcode;
    int m;
    int n;
    int nnz;
    int *row_ind;
    int *col_ind;
    double *val;


    // load and expand sparse matrix from file (if symmetric)
    fprintf(stdout, "Matrix file name: %s ... ", matrixName);
    t0 = ReadTSC();
    ret = mm_read_mtx_crd(matrixName, &m, &n, &nnz, &row_ind, &col_ind, &val, 
                          &matcode);
    check_mm_ret(ret);
    if(is_symmetric) {
        expand_symmetry(m, n, &nnz, &row_ind, &col_ind, &val);
    }
    timer[LOAD_TIME] += ElapsedTime(ReadTSC() - t0);

    
    // Convert co-ordinate format to CSR format
    fprintf(stdout, "Converting COO to CSR...");
    unsigned int* csr_row_ptr = NULL; 
    unsigned int* csr_col_ind = NULL;  
    double* csr_vals = NULL; 
    t0 = ReadTSC();
    // IMPLEMENT THIS FUNCTION - MAKE SURE IT'S PARALLELIZED
    convert_coo_to_csr(row_ind, col_ind, val, m, n, nnz,
                       &csr_row_ptr, &csr_col_ind, &csr_vals);
    timer[CONVERT_TIME] += ElapsedTime(ReadTSC() - t0);
    fprintf(stdout, "done\n");


    // Load the input vector file
    char vectorName[MAX_FILENAME];
    strcpy(vectorName, argv[2]);
    fprintf(stdout, "Vector file name: %s ... ", vectorName);
    double* vector_x;
    unsigned int vector_size;
    t0 = ReadTSC();
    read_vector(vectorName, &vector_x, &vector_size);
    timer[LOAD_TIME] += ElapsedTime(ReadTSC() - t0);
    assert(n == vector_size);
    fprintf(stdout, "file loaded\n");


    // Calculate COO SpMV
    // first set up some locks
    t0 = ReadTSC();
    omp_lock_t* writelock; 
    init_locks(&writelock, m);
    timer[LOCK_INIT_TIME] += ElapsedTime(ReadTSC() - t0);

    double *res_coo = (double*) malloc(sizeof(double) * m);;
    assert(res_coo);
    fprintf(stdout, "Calculating COO SpMV ... ");
    t0 = ReadTSC();
    for(unsigned int i = 0; i < MAX_ITER; i++) {
        // IMPLEMENT THIS FUNCTION - MAKE SURE IT'S PARALLELIZED 
        spmv_coo(row_ind, col_ind, val, m, n, nnz, vector_x, res_coo, 
                 writelock);
    }
    timer[SPMV_COO_TIME] += ElapsedTime(ReadTSC() - t0);
    fprintf(stdout, "done\n");

    /*-------------------------------------DEBUG SECTION--------------------------------------------------*/
    //setup variables needed
    //construct_csr();
    /*-------------------------------------END DEBUG SECTION--------------------------------------------------*/

    // Calculate CSR SpMV
    double *res_csr = (double*) malloc(sizeof(double) * m);;
    assert(res_csr);
    fprintf(stdout, "Calculating CSR SpMV ... ");
    t0 = ReadTSC();
    for(unsigned int i = 0; i < MAX_ITER; i++) {
        // IMPLEMENT THIS FUNCTION - MAKE SURE IT'S PARALLELIZED 
        spmv(csr_row_ptr, csr_col_ind, csr_vals, m, n, nnz, vector_x, res_csr);
    }
    timer[SPMV_CSR_TIME] += ElapsedTime(ReadTSC() - t0);
    fprintf(stdout, "done\n");

    // Store the calculated vector in a file, one element per line.
    char resName[MAX_FILENAME];
    strcpy(resName, argv[3]); 
    fprintf(stdout, "Result file name: %s ... ", resName);
    t0 = ReadTSC();
    store_result(resName, res_coo, m);
    // store_result(resName, res_coo, m);
    timer[STORE_TIME] += ElapsedTime(ReadTSC() - t0);
    fprintf(stdout, "file saved\n");


    // print timer
    print_time(timer);


    // Free memory
    free(csr_row_ptr);
    free(csr_col_ind);
    free(csr_vals);
    free(vector_x);
    free(res_coo);
    free(res_csr);
    free(row_ind);
    free(col_ind);
    free(val);
    destroy_locks(writelock, m);

    return 0;
}


/* This function checks the number of input parameters to the program to make 
   sure it is correct. If the number of input parameters is incorrect, it 
   prints out a message on how to properly use the program.
   input parameters:
       int    argc
       char** argv 
   return parameters:
       none
 */
void usage(int argc, char** argv)
{
    if(argc < 4) {
        fprintf(stderr, "usage: %s <matrix> <vector> <result>\n", argv[0]);
        exit(EXIT_FAILURE);
    } 
}

/* This function prints out information about a sparse matrix
   input parameters:
       char*       fileName    name of the sparse matrix file
       MM_typecode matcode     matrix information
       int         m           # of rows
       int         n           # of columns
       int         nnz         # of non-zeros
   return paramters:
       none
 */
void print_matrix_info(char* fileName, MM_typecode matcode, 
                       int m, int n, int nnz)
{
    fprintf(stdout, "-----------------------------------------------------\n");
    fprintf(stdout, "Matrix name:     %s\n", fileName);
    fprintf(stdout, "Matrix size:     %d x %d => %d\n", m, n, nnz);
    fprintf(stdout, "-----------------------------------------------------\n");
    fprintf(stdout, "Is matrix:       %d\n", mm_is_matrix(matcode));
    fprintf(stdout, "Is sparse:       %d\n", mm_is_sparse(matcode));
    fprintf(stdout, "-----------------------------------------------------\n");
    fprintf(stdout, "Is complex:      %d\n", mm_is_complex(matcode));
    fprintf(stdout, "Is real:         %d\n", mm_is_real(matcode));
    fprintf(stdout, "Is integer:      %d\n", mm_is_integer(matcode));
    fprintf(stdout, "Is pattern only: %d\n", mm_is_pattern(matcode));
    fprintf(stdout, "-----------------------------------------------------\n");
    fprintf(stdout, "Is general:      %d\n", mm_is_general(matcode));
    fprintf(stdout, "Is symmetric:    %d\n", mm_is_symmetric(matcode));
    fprintf(stdout, "Is skewed:       %d\n", mm_is_skew(matcode));
    fprintf(stdout, "Is hermitian:    %d\n", mm_is_hermitian(matcode));
    fprintf(stdout, "-----------------------------------------------------\n");

}


/* This function checks the return value from the matrix read function, 
   mm_read_mtx_crd(), and provides descriptive information.
   input parameters:
       int ret    return value from the mm_read_mtx_crd() function
   return paramters:
       none
 */
void check_mm_ret(int ret)
{
    switch(ret)
    {
        case MM_COULD_NOT_READ_FILE:
            fprintf(stderr, "Error reading file.\n");
            exit(EXIT_FAILURE);
            break;
        case MM_PREMATURE_EOF:
            fprintf(stderr, "Premature EOF (not enough values in a line).\n");
            exit(EXIT_FAILURE);
            break;
        case MM_NOT_MTX:
            fprintf(stderr, "Not Matrix Market format.\n");
            exit(EXIT_FAILURE);
            break;
        case MM_NO_HEADER:
            fprintf(stderr, "No header information.\n");
            exit(EXIT_FAILURE);
            break;
        case MM_UNSUPPORTED_TYPE:
            fprintf(stderr, "Unsupported type (not a matrix).\n");
            exit(EXIT_FAILURE);
            break;
        case MM_LINE_TOO_LONG:
            fprintf(stderr, "Too many values in a line.\n");
            exit(EXIT_FAILURE);
            break;
        case MM_COULD_NOT_WRITE_FILE:
            fprintf(stderr, "Error writing to a file.\n");
            exit(EXIT_FAILURE);
            break;
        case 0:
            fprintf(stdout, "file loaded.\n");
            break;
        default:
            fprintf(stdout, "Error - should not be here.\n");
            exit(EXIT_FAILURE);
            break;

    }
}

/* This function reads information about a sparse matrix using the 
   mm_read_banner() function and printsout information using the
   print_matrix_info() function.
   input parameters:
       char*       fileName    name of the sparse matrix file
   return paramters:
       none
 */
void read_info(char* fileName, int* is_sym)
{
    FILE* fp;
    MM_typecode matcode;
    int m;
    int n;
    int nnz;

    if((fp = fopen(fileName, "r")) == NULL) {
        fprintf(stderr, "Error opening file: %s\n", fileName);
        exit(EXIT_FAILURE);
    }

    if(mm_read_banner(fp, &matcode) != 0)
    {
        fprintf(stderr, "Error processing Matrix Market banner.\n");
        exit(EXIT_FAILURE);
    } 

    if(mm_read_mtx_crd_size(fp, &m, &n, &nnz) != 0) {
        fprintf(stderr, "Error reading size.\n");
        exit(EXIT_FAILURE);
    }

    print_matrix_info(fileName, matcode, m, n, nnz);
    *is_sym = mm_is_symmetric(matcode);

    fclose(fp);
}

/* This function converts a sparse matrix stored in COO format to CSR format.
   input parameters:
       int*	row_ind		list or row indices (per non-zero)
       int*	col_ind		list or col indices (per non-zero)
       double*	val		list or values  (per non-zero)
       int	m		# of rows
       int	n		# of columns
       int	nnz		# of non-zeros
   output parameters:
       unsigned int** 	csr_row_ptr	pointer to row pointers (per row)
       unsigned int** 	csr_col_ind	pointer to column indices (per non-zero)
       double** 	csr_vals	pointer to values (per non-zero)
   return paramters:
       none
 */
void convert_coo_to_csr(int* row_ind, int* col_ind, double* val, 
                        int m, int n, int nnz,
                        unsigned int** csr_row_ptr, unsigned int** csr_col_ind,
                        double** csr_vals)

{
    //setup the variables needed
    int binCount = m + 1;
    int histo[binCount];
    int* src;
    int* prefix;

    //allocate memory for prefix array
    prefix = (int*)malloc(sizeof(int)*binCount); 
    
    //assign value to new array
    for (int i = 0; i < nnz - 1; i++) {
        //csr_row_ptr[row_ind[i] + 1] = csr_row_ptr[row_ind[i] + 1] + 1;
        csr_vals[i] = &val[i];
        csr_col_ind[i] = &col_ind[i];
        printf("%f value at index %d and (%d, %d)\n", *csr_vals[i], i, row_ind[i], *csr_col_ind[i]);
    }

    //histogram for bin count
    for (int i = 0; i < binCount; i++) {
        histo[i] = 0;
    }
    
    //check zero out histogram
    for (int i = 0; i < binCount; i++) {
        printf("histogram bin at index %d is %d\n", i, histo[i]);
    }

    //sort indexes into bins
    for (int i = 0; i < nnz; i++) {
        printf("row_ind[%d] = %d\n",i,row_ind[i]);
        histo[row_ind[i]]++;
    }
    
    //check complete histogram
    for (int i = 0; i < binCount; i++) {
        printf("histogram bin at index %d is %d\n", i, histo[i]);
    }

    src = histo;

    //init prefix with zeroes
    for (int i = 0; i < binCount; i++) {
        prefix[i] = 0;
        printf("prefix bin at index %d is %d\n", i, prefix[i]);
    }

    //run the prefix sum
    prefix_sum_p2(src, prefix, binCount);
}

void prefix_sum_p2(int* src, int* prefix, int n)
{
    // copy input to the prefix array
    memcpy(prefix, src, sizeof(int) * n);
 
    // reduce step
    for(int i = 1; i < n; i = i * 2) {
        //reduce
        #pragma omp parallel for schedule(static)
        for(int j = (i * 2) - 1; j < n; j += (i * 2)) {
            prefix[j] += prefix[j - i];
        }
    }

    // Down sweep step
    for(int i = n / 4; i > 0; i = i / 2) {
        // reduce
        #pragma omp parallel for schedule(static)
        for(int j = (i * 2) - 1; j  < n - i; j += (i * 2)) {
            prefix[j + i] += prefix[j];
        }
    }

    //check the prefix sum
    for (int i = 0; i < n; i++) {
        printf("prefix at index %d is %d\n", i, prefix[i]);
    }
}

/* Reads in a vector from file.
   input parameters:
       char*	fileName	name of the file containing the vector
   output parameters:
       double**	vector		pointer to the vector
       int*	vecSize 	pointer to # elements in the vector
   return parameters:
       none
 */
void read_vector(char* fileName, double** vector, int* vecSize)
{
    FILE* fp = fopen(fileName, "r");
    assert(fp);
    char line[MAX_NUM_LENGTH];    
    fgets(line, MAX_NUM_LENGTH, fp);
    fclose(fp);

    unsigned int vector_size = atoi(line);
    double* vector_ = (double*) malloc(sizeof(double) * vector_size);

    fp = fopen(fileName, "r");
    assert(fp); 
    // first read the first line to get the # elements
    fgets(line, MAX_NUM_LENGTH, fp);

    unsigned int index = 0;
    while(fgets(line, MAX_NUM_LENGTH, fp) != NULL) {
        vector_[index] = atof(line); 
        index++;
    }

    fclose(fp);
    assert(index == vector_size);

    *vector = vector_;
    *vecSize = vector_size;
}

/* SpMV function for COO stored sparse matrix
 */
void spmv_coo(unsigned int* row_ind, unsigned int* col_ind, double* vals, 
              int m, int n, int nnz, double* vector_x, double *res, 
              omp_lock_t* writelock)
{
    //variables for loop
    int I = 0;
    int J = 0;
    int i = 0;
    double val = 0.0;
    double tmp = 0.0;

    #pragma omp for schedule(static,124)    //fastest implementation so far
    for (int i = 0; i < m; ++i) {
        res[i] = 0.0;
    }
    //print_array(res, m);

    #pragma omp for schedule(static,124)    //fastest implementation so far
    for (i = 0; i < nnz; ++i) {
        I = row_ind[i] - 1;
        J = col_ind[i];
        val = vals[i];

        tmp += (vector_x[J - 1] * val);
        res[i] += tmp;
    }
    //printf("spmv_coo finished\n");
    //print_array(res, 10);
}

void print_array(double *array, int m)
{
    printf("print_array:\n");
    for (int i = 0; i <= m; i++) {
        printf("%f at index %d\n", array[i], i);
    }
}

void construct_csr()
{
    printf("constructing csr matrix...\n");
    /*Test Matrix
    (5,0,0,0)
    (0,8,0,0)
    (0,0,3,0)
    (0,6,0,0)
    */

    int nnz_test = 0;
    int m = 4;
    int n = 4;
    int nnz = 4;
    double* csr_vals = (double*) malloc(sizeof(double) * m);
    double* vector_x = (double*) malloc(sizeof(double) * m);
    unsigned int* csr_col_ind = (unsigned int*) malloc(sizeof(double) * m);
    unsigned int* csr_row_ptr = (unsigned int*) malloc(sizeof(double) * m);
    double* res = (double*) malloc(sizeof(double) * m);

    //setup array of values
    double valArray[4] = {5,8,3,6};
    csr_vals = valArray;

    //setup row_ptr
    unsigned int row_ptr[5] = {0,1,2,3,4};
    csr_row_ptr = row_ptr;
    

    //setup vector to multiply
    double vectorArray[4] = {1,1,1,0};
    vector_x = vectorArray;
    
    //setup array of indexes for non-zero elements
    unsigned int colArray[4] = {0,1,2,1};
    csr_col_ind = colArray;

    //setup array for res
    double resArray[4] = {0,0,0,0};
    res = resArray;

    spmv(csr_row_ptr, csr_col_ind, csr_vals, m, n, nnz, vector_x, res);
}


/* SpMV function for CSR stored sparse matrix
 */
void spmv(unsigned int* csr_row_ptr, unsigned int* csr_col_ind, 
          double* csr_vals, int m, int n, int nnz, 
          double* vector_x, double *res)
{
    //for (int i = 0; i < m; i++) {
    //    printf("csr_vals: %f at index %d        ", csr_vals[i], i);
    //    printf("csr_col_ind: %d at index %d        ", csr_col_ind[i], i);
    //    printf("csr_vals: %f at index %d        \n", csr_vals[i], i);
    //}

    //for (int i = 0; i < n; i++) {
    //    for (int k = csr_row_ptr[i]; k < csr_row_ptr[i+1]; k++) {
    //        res[i] = res[i] + csr_vals[k] * vector_x[csr_col_ind[k]];
    //        printf("%f = %f + %f * %f\n"
    //        ,res[i], res[i], csr_vals[k], vector_x[csr_col_ind[k]]);
    //    }
    //    printf("\n");
    //}

    //for (int i = 0; i < m; i++) {
    //    printf("res: %f at index %d        \n", res[i], i);
    //}
    printf("spmv done\n");
}


/* Save result vector in a file
 */
void store_result(char *fileName, double* res, int m)
{
    FILE* fp = fopen(fileName, "w");
    assert(fp);

    fprintf(fp, "%d\n", m);
    for(int i = 0; i < m; i++) {
        fprintf(fp, "%0.10f\n", res[i]);
    }

    fclose(fp);
}

/* Print timing information 
 */
void print_time(double timer[])
{
    fprintf(stdout, "Module\t\tTime\n");
    fprintf(stdout, "Load\t\t");
    fprintf(stdout, "%f\n", timer[LOAD_TIME]);
    fprintf(stdout, "Convert\t\t");
    fprintf(stdout, "%f\n", timer[CONVERT_TIME]);
    fprintf(stdout, "Lock Init\t");
    fprintf(stdout, "%f\n", timer[LOCK_INIT_TIME]);
    fprintf(stdout, "COO SpMV\t");
    fprintf(stdout, "%f\n", timer[SPMV_COO_TIME]);
    fprintf(stdout, "CSR SpMV\t");
    fprintf(stdout, "%f\n", timer[SPMV_CSR_TIME]);
    fprintf(stdout, "Store\t\t");
    fprintf(stdout, "%f\n", timer[STORE_TIME]);
}

void expand_symmetry(int m, int n, int* nnz_, int** row_ind, int** col_ind, 
                     double** val)
{
    fprintf(stdout, "Expanding symmetric matrix ... ");
    int nnz = *nnz_;

    // first, count off-diagonal non-zeros
    int not_diag = 0;
    for(int i = 0; i < nnz; i++) {
        if((*row_ind)[i] != (*col_ind)[i]) {
            not_diag++;
        }
    }

    int* _row_ind = (int*) malloc(sizeof(int) * (nnz + not_diag));
    assert(_row_ind);
    int* _col_ind = (int*) malloc(sizeof(int) * (nnz + not_diag));
    assert(_col_ind);
    double* _val = (double*) malloc(sizeof(double) * (nnz + not_diag));
    assert(_val);

    memcpy(_row_ind, *row_ind, sizeof(int) * nnz);
    memcpy(_col_ind, *col_ind, sizeof(int) * nnz);
    memcpy(_val, *val, sizeof(double) * nnz);
    int index = nnz;
    for(int i = 0; i < nnz; i++) {
        if((*row_ind)[i] != (*col_ind)[i]) {
            _row_ind[index] = (*col_ind)[i];
            _col_ind[index] = (*row_ind)[i];
            _val[index] = (*val)[i];
            index++;
        }
    }
    assert(index == (nnz + not_diag));

    free(*row_ind);
    free(*col_ind);
    free(*val);

    *row_ind = _row_ind;
    *col_ind = _col_ind;
    *val = _val;
    *nnz_ = nnz + not_diag;

    fprintf(stdout, "done\n");
    fprintf(stdout, "  Total # of non-zeros is %d\n", nnz + not_diag);
}

void init_locks(omp_lock_t** locks, int m)
{
    omp_lock_t* _locks = (omp_lock_t*) malloc(sizeof(omp_lock_t) * m);
    assert(_locks);
    for(int i = 0; i < m; i++) {
        omp_init_lock(&(_locks[i]));
    }
    *locks = _locks;
}

void destroy_locks(omp_lock_t* locks, int m)
{
    assert(locks);
    for(int i = 0; i < m; i++) {
        omp_destroy_lock(&(locks[i]));
    }
    free(locks);
}