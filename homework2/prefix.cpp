#include <stdio.h>
#include <cmath>

void prefix_sum(int src[], int prefix[], int n);
void prefix_sum_p1(int src[], int prefix[], int n);
void prefix_sum_p2(int src[], int prefix[], int n);

int main (int argc, char** argv)
{
    int src[8] = {6,4,16,10,16,14,2,8};
    int prefix[8];
    //prefix_sum(src, prefix, 8);
    //prefix_sum_p1(src, prefix, 8);
    prefix_sum_p2(src, prefix, 8);
}

void prefix_sum(int src[], int prefix[], int n)
{
    prefix[0] = src[0];
    for (int i = 1; i < n; i++) {
        prefix[i] = src[i] + prefix[i - 1];
        printf("%d = %d + %d\n",prefix[i], src[i], prefix[i-1]);
    }
    for (int i = 0; i < n; i++) {
        printf("%d ",prefix[i]);
    }
    printf("\n");
}

void prefix_sum_p1(int src[], int prefix[], int n)
{
    prefix[0] = src[0];
    for(int i = 1; i < n; i = i * 2) {
        printf("i = %d\n",i);
        for (int j = 0; j < n; j+=2^i) {
            printf("j = %d\n",j);
            prefix[j] = src[j] + src[j + i];
            printf("%d = %d + %d\n", prefix[j], src[j], src[j + i]);
        }
        for (int i = 0; i < n; i++) {
            printf("%d ",prefix[i]);
        }
    printf("\n");
    }
    
    for (int i = 0; i < n; i++) {
        printf("%d ",prefix[i]);
    }
    printf("\n");
}

void prefix_sum_p2(int src[], int prefix[], int n)
{
    for (int i = 0; i < ceil(log2(n)); i++) {
        for (int j = 0; j < n; j++) {
            if (j < 2^i)
                prefix[j] = src[j];
            else
                prefix[j] = src[j] + prefix[j - 2^i];

        }
        int *tmp = prefix;
        prefix = src;
        src = tmp;
    }
}