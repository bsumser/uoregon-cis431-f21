#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "common.h"
#include <inttypes.h>
#include <time.h>


#define PI 3.1415926535


void usage(int argc, char** argv);
double calcPi_Serial(int num_steps);
double calcPi_P1(int num_steps);
double calcPi_P2(int num_steps);
double calcPi_Monte_Carlo(int num_steps);

int main(int argc, char** argv)
{
    // get input values
    uint32_t num_steps = 100;
    if(argc > 1) {
        num_steps = atoi(argv[1]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32"\n", num_steps);
    }
    fprintf(stdout, "The first 10 digits of Pi are %0.10f\n", PI);


    // set up timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    // calculate in serial 
    start_t = ReadTSC();
    double Pi0 = calcPi_Serial(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi serially with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi0);
    
    // calculate in parallel with reduce 
    start_t = ReadTSC();
    double Pi1 = calcPi_P1(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi1);


    // calculate in parallel with atomic add
    start_t = ReadTSC();
    double Pi2 = calcPi_P2(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // + atomic with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi2);

    // calculate with Monte Carlo Method
    start_t = ReadTSC();
    double Pi3 = calcPi_Monte_Carlo(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in Monte Carlo with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi3);
    
    return 0;
}


void usage(int argc, char** argv)
{
    fprintf(stdout, "usage: %s <# steps>\n", argv[0]);
}

double calcPi_Serial(int num_steps)
{
    double pi = 0.0;
    double indexArea = 0.0;
    double totalArea = 0.0;
    double innerRoot = 0.0;

    for (int i = 0; i <= num_steps; i++)
    {

        innerRoot = pow(((3 * (double) i) / num_steps),2.0);

        indexArea = (3 / (double) num_steps) * (sqrt(9 - innerRoot));
        totalArea += indexArea;

        //printf("innerRoot = %f, indexArea = %f, totalArea = %f\n",innerRoot, indexArea, totalArea);
    }

    totalArea = totalArea * 4;

    pi = totalArea / (pow(3,2));
    
    return pi;
}

double calcPi_P1(int num_steps)
{
    double pi = 0.0;
    double indexArea = 0.0;
    double totalArea = 0.0;
    double innerRoot = 0.0;

    #pragma omp parallel for reduction(+:totalArea)
    for (int i = 0; i <= num_steps; i++)
    {
        innerRoot = pow(((3 * (double) i) / num_steps),2.0);

        indexArea = (3 / (double) num_steps) * (sqrt(9 - innerRoot));

        totalArea += indexArea;

        //printf("innerRoot = %f, indexArea = %f, totalArea = %f\n",innerRoot, indexArea, totalArea);
    }

    totalArea = totalArea * 4;

    pi = totalArea / (pow(3,2));
    
    return pi;
}

double calcPi_P2(int num_steps)
{
    double pi = 0.0;

    return pi;
}

double calcPi_Monte_Carlo(int num_steps)
{
    double pi = 0.0;
    int MAX_SIZE = num_steps;

    double randX = 0;
    double randY = 0;

    double distance = 0;

    int pointsInside = 0;
    int pointsOutside = 0;

    for (int i = 0; i < MAX_SIZE; i++)
    {
        randX = (double) rand()/RAND_MAX;
        randY = (double) rand()/RAND_MAX;
        distance = sqrt(pow((randX - 0.5),2.0) + pow((randY - 0.5),2.0));

        //std::cout << "Point is (" << randX << "," << randY << ")" << 
        //" at "<< distance << " from center";
        
        if (distance >= 0.5)
        {
            //std::cout << " point is outisde" << std::endl;
            pointsOutside++;
        }
        else
        {
            //std::cout << " point is inside" << std::endl;
            pointsInside++;
        }
    }

    //std::cout << pointsInside << " points inside" << std::endl;
    //std::cout << pointsOutside << " points outside" << std::endl;

    pi = 4 * ((double) pointsInside / ( (double) pointsInside + (double) pointsOutside));

    return pi;
}